#!/bin/env python3

import feedparser
import json
from easydict import EasyDict as edict

parsed_feed = edict(feedparser.parse("http://www.nyaa.se/?page=rss&term=DLYS"))
#print(json.dumps(parsed_feed, sort_keys=True, indent=4, ensure_ascii=False))

for i in parsed_feed.entries:
    print("{id}: {title}".format(id = i.id.split("=")[-1], title = i.title))
    print 
