#!/bin/env python3

import json
import sys
from easydict import EasyDict as edict          # https://github.com/makinacorpus/easydict.git

from gettorrent import Feed



config = edict(json.loads(open('gettorrent.json').read()))

print(json.dumps(config, sort_keys=True, indent=4, ensure_ascii=False))

Feed.verbose = False
a = Feed()
a.rss_url = config.trackers.nyaa.rss_url
a.dnd = False


for i in config.tracks:
    my_feed = Feed()
    tracker = config.trackers[i.tracker]
    my_feed.rss_url = tracker.rss_url
    my_feed.keywords = "+".join(i.keywords + tracker.keywords)
    my_feed.get_url_list()
#    for a in my_feed.url_list:
#        print(a[1].split('=')[-1],a[0],a[1])
    

try:
    if "--dnd" in sys.argv:
        dnd = True
except:
    pass


