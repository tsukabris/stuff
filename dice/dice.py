#!/bin/env python3

from random import randrange
import sys


def stats():
    stats = []
    mods = []
    result = []
    for i in range(6):
        pool = [randrange(1, 7) for i in range(4)]
        pool.sort()
        stat = sum(pool[1:4])
        mod = (stat - 10) // 2
        result.append("{}: {} ({})".format(", ".join([str(s) for s in pool]), str(stat), str(mod)))
        stats.append(stat)
        mods.append(mod)
    result.append("{} {}".format(str(round(sum(stats)/6, 3)), str(sum(mods))))
    return result


def roll(args):
    rolls, die = [int(i) for i in args.split("d")]
    pool = [randrange(1, die+1) for i in range(rolls)]
    pool.sort()
    print("{}={}".format("+".join([str(s) for s in pool]), sum(pool)))
   

def roll_vtm(args):
    rolls, diff = [int(i) for i in args.split(" ")]
    if diff > 10: diff = 10
    pool = [randrange(1, 11) for i in range(int(rolls))]
    print("Rolling {} d10 with DC {}.".format(rolls, diff))
    pool.sort()
    failure = len([i for i in pool if i == 1])
    success = len([i for i in pool if i >= int(diff)])
    if success == 0 and failure > 0:
        result = "{}x botch!".format(failure)
    elif success > 0 and success > failure:
        result = "{}x success!".format(success-failure)
    else:
        result = "Fail."
    
    print("{}\n{}\n".format(" ".join([str(s) for s in pool]), result))

def help():
    print("exit,e,quit,q - exit the program")
    print("<number>d<number> - roll the dices")
    print("stats,s - roll the dices for DnD stats")
    

