#!/bin/env/python

import dice
import re

re_dice = re.compile("^\d{1,}d\d{1,}")
re_dice_vtm = re.compile("^\d{1,}\s\d{1,}")

lastroll = "1d20"
while True:
    command = input()
    if command == "":
        command = lastroll
    if command in ["exit", "e", "quit", "q"]:
        exit()
    elif re_dice.match(command):
        dice.roll(command)
    elif re_dice_vtm.match(command):
        dice.roll_vtm(command)
    elif command in ["stats", "s"]:
        print(dice.stats())
    elif command in ["help", "h"]:
        dice.help()
    lastroll = command
